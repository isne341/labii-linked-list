#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('3');
	mylist.pushToHead('2');
	mylist.pushToHead('1');
	mylist.print();
	cout << endl;
	mylist.reverse();
	mylist.print();

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!
	
}